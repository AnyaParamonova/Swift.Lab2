//
//  Animals.swift
//  TabBarApplication
//
//  Created by Admin on 17.02.17.
//  Copyright © 2017 paranastasia. All rights reserved.
//

import Foundation

class Animal {
    
    var name : String = ""
    var picturePath : String = ""
    var description : String = ""
    
    
    init(name : String, picturePath : String, description : String){
        
        self.name = name
        self.picturePath = picturePath
        self.description = description
    }
}
